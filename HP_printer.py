#V1.0: 12.04.2020: first version without controls (try except)
#V1.1: 13.04:2020: added controls 
#V1.2: 14.04.2020: added watchdog that update a file

import smtplib, imaplib, email
import logging
import sys

# login information where the email must be feched
imap_host = "MYHOSTING.com"
user_imap = "IMAP_USERNAME@MYSITE.COM"
passwd_imap = "MY_IMAP_PASSWORD"

# login information of the smtp mail provider where mail has to be sent 
smtp_host = "MYSMTP_HOST.com"
smtp_port = 587
user_smtp = "SMTP_USERNAME@MYSITE.COM"
passwd_smtp = "MY_SMTP_PASSWORD"
msgid = 1
# Email information to forward the orginal MAIL as a copy (not necessary only for debug)
from_addr_forward ="FROM_FORWARD@FROMEMAIL.COM"
to_addr_forward ="FORWARD@MYFORWARDEMAIL.COM"

# Cloud Email Address of the HP Printer

#Email where the confirmation of the printed document has to sent
from_addr = "FROM_FORWARD@FROMEMAIL.COM"
#HP CLOUD Printer EMAIL
to_addr = "MYCODE@hpeprint.com"

logging.basicConfig(filename='forward.log',
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S,%d,%m,%Y',
                            level=logging.DEBUG)

logging.info('forward script started')

#reset watchdog writing to a file
f = open("watchdog.txt", "w")
f.write("Watchdog file. Will be rewritten at every execution of forward.py\n")
f.write("Watchdog verifies that the file is not older than 900s = 15min\n")
f.write("settings are in file etc watchdog.conf\n")
f.close()


# open IMAP connection and fetch only 1 message with id msgid
# store message data in email_data
client = imaplib.IMAP4(imap_host)
try:
   client.login(user_imap, passwd_imap)
except imaplib.IMAP4.error: #except an error and exit
   logging.ERROR('LOGIN IMAP Failed')
   sys.exit(1)
client.select('INBOX')
typ, data = client.search(None, 'ALL')
typ, data = client.fetch(msgid, "(RFC822)")
#print data
if data == [None]: 
    logging.info("The mailbox is empty")
    logging.info("Exit program\n")
    client.close()
    client.logout()
    sys.exit(0)
logging.info("Found 1 Email in the mailbox")
email_data = data[0][1]
#delete message in inbox
client.store(msgid, '+FLAGS', '\\Deleted')
client.expunge()
client.close()
client.logout()

# create a Message instance from the email data
message = email.message_from_string(email_data)

#change FROM to have the same sender

message.replace_header("From", from_addr_forward)
message.replace_header("To", to_addr_forward)
 
#forward original message
smtp = smtplib.SMTP(smtp_host, smtp_port)
smtp.starttls()
try:
   smtp.login(user_smtp, passwd_smtp)
except imaplib.IMAP4.error: #except an error and exit
   logging.ERROR('LOGIN SMTP 1 Failed')
   sys.exit(1)
smtp.sendmail(from_addr_forward, to_addr_forward, message.as_string())
smtp.quit()

# replace headers (could do other processing here)
message.replace_header("From", from_addr)
message.replace_header("To", to_addr)

# open authenticated SMTP connection and send message with
# specified envelope from and to addresses
smtp = smtplib.SMTP(smtp_host, smtp_port)
smtp.starttls()
try:
   smtp.login(user_smtp, passwd_smtp)
except imaplib.IMAP4.error: #except an error and exit
   logging.ERROR('LOGIN SMTP 2 Failed')
   sys.exit(1)
smtp.sendmail(from_addr, to_addr, message.as_string())
smtp.quit()
